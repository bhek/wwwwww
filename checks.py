from curses import *
from curses import panel
import sprites

def hazard_check(y, x, MAP):
    d = False
    
    if MAP[y][x] == sprites.hazard:
        d = True
    elif MAP[y + 1][x] == sprites.hazard:
        d = True
    elif MAP[y][x] == sprites.hazard:
        d = True

    return d

def flipper_check(f, y, x, MAP):

    if MAP[y][x] == sprites.flipper:
        f = not f
    elif MAP[y + 1][x] == sprites.flipper:
        f = True
    elif MAP[y][x] == sprites.flipper:
        f = False

    return f

def next_board(y, x, MAP):
    p = False

    if MAP[y][x] == sprites.forward:
        p = True
    elif MAP[y + 1][x] == sprites.forward:
        p = True
    elif MAP[y][x] == sprites.forward:
        p = True

    return p

def prev_board(y, x, MAP):
    p = False

    if MAP[y][x] == sprites.backward:
        p = True
    elif MAP[y + 1][x] == sprites.backward:
        p = True
    elif MAP[y][x] == sprites.backward:
        p = True

    return p

def winning_check(y, x, MAP):
    w = False

    if MAP[y][x] == sprites.end_level:
        w = True
    elif MAP[y + 1][x] == sprites.end_level:
        w = True
    elif MAP[y - 1][x] == sprites.end_level:
        w = True

    return w